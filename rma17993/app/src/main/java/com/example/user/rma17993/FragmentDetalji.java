package com.example.user.rma17993;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class FragmentDetalji extends Fragment implements View.OnClickListener {
    private Muzicar muzjo;
    Boolean pamti;
    Button dugme;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View iv =  inflater.inflate(R.layout.detalji_fragment, container, false);

        dugme = (Button)iv.findViewById(R.id.button2);
        dugme.setOnClickListener(this);
        pamti = false;
        if (getArguments()!= null && getArguments().containsKey("muzicar")){
            muzjo= getArguments().getParcelable("muzicar");
            TextView ime = (TextView) iv.findViewById(R.id.textView);
            TextView zanr = (TextView) iv.findViewById(R.id.textView2);
            //Postavljanje i ostalih vrijednosti na isti način
            ime.setText(muzjo.getNaziv());
            zanr.setText(muzjo.getZanr());

        }
        return iv;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button2:
                if (!pamti){
                    //Slučaj za top 5

                    Bundle arguments = new Bundle();
                    arguments.putStringArrayList("lista", muzjo.dajListu());

                    FragmentTransaction ft = getChildFragmentManager().beginTransaction();
                    FragmentTop5Pjesama f5 = new FragmentTop5Pjesama();
                    f5.setArguments(arguments);
                    ft.replace(R.id.frameLayout, f5);
                    ft.addToBackStack(null);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                    pamti = true;
                }
                else {
                    //Slučaj za top 5 sličnih muzičara
                    FragmentTransaction ft = getChildFragmentManager().beginTransaction();
                    FragmentTop5Pjevaca f5p = new FragmentTop5Pjevaca();
                    ft.replace(R.id.frameLayout, f5p);
                    ft.addToBackStack(null);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                    pamti = false;
                }
                break;
        }
    }
}
