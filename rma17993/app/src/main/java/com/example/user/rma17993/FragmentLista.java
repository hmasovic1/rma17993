package com.example.user.rma17993;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import java.util.ArrayList;

import static com.spotify.sdk.android.authentication.LoginActivity.REQUEST_CODE;

public class FragmentLista extends Fragment implements SearchArtist.onMuzicarSearchDone, View.OnClickListener, ResultRecieve.Receiver {

    OnItemClick oic;
    ArrayList<Muzicar> muzicari;
    ListView lv;
    EditText ex;
    Button b;

    ResultRecieve mReceiver = null;

    private static final int REQUEST_CODE = 1337;
    private static final String REDIRECT_URI = "yourcustomprotocol://callback";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_lista, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ex = (EditText) getView().findViewById(R.id.editText);
        lv = (ListView) getView().findViewById(R.id.listaMuzicara);
        b = (Button) getView().findViewById(R.id.button4);
        b.setOnClickListener(this);


        if (getArguments().containsKey("Alista")) {
            muzicari = getArguments().getParcelableArrayList("Alista");
            MuzicarAdapter aa = new MuzicarAdapter(getActivity(), muzicari);
            lv.setAdapter(aa);
        }

        try {
            oic = (OnItemClick)getActivity();
        }catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString() + "Treba implementirati OnItemClick");
        }

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                oic.onItemClicked(i, muzicari);
            }
        });

    }

    @Override
    public void onDone(ArrayList<Muzicar> lista) {
        muzicari = lista;
        if(lista != null) {
            MuzicarAdapter aa = new MuzicarAdapter(getActivity(), muzicari);
            lv.setAdapter(aa);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button4:
           //     uradi();
                Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), SearchArtistServis.class);
                intent.putExtra("zahtjev",ex.getText().toString());

                mReceiver = new ResultRecieve(new Handler());
                mReceiver.setReceiver(FragmentLista.this);

                intent.putExtra("receiver", mReceiver);
                getActivity().startService(intent);
                break;

        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode){
            case 1:
                muzicari = (ArrayList<Muzicar>) resultData.getSerializable("result");
                MuzicarAdapter aa = new MuzicarAdapter(getActivity(), muzicari);
                lv.setAdapter(aa);
                break;
        }
    }


    public interface OnItemClick {
        public void onItemClicked(int pos, ArrayList<Muzicar> m);
    }

    private void uradi(){
        AuthenticationRequest.Builder builder =
                new AuthenticationRequest.Builder("v9kt78urswesmix4bequmitvh", AuthenticationResponse.Type.TOKEN, REDIRECT_URI);

        builder.setScopes(new String[]{"streaming"});
        AuthenticationRequest request = builder.build();

        AuthenticationClient.openLoginActivity(getActivity(), REQUEST_CODE, request);
    }

    String tokencic = null;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, data);

            switch (response.getType()) {
                // Response was successful and contains auth token
                case TOKEN:
                    // Handle successful response
                    tokencic = response.getAccessToken();
                    break;

                // Auth flow returned an error
                case ERROR:
                    // Handle error response
                    break;

                // Most likely auth flow was cancelled
                default:
                    // Handle other cases
            }
        }
    }
}

