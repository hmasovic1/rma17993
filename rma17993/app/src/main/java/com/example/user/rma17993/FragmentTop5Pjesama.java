package com.example.user.rma17993;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class FragmentTop5Pjesama extends Fragment {

    ListView lw;
    ArrayAdapter<String> adapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View iv = inflater.inflate(R.layout.top5_pjesama, container, false);

        lw = (ListView) iv.findViewById(R.id.listView);
        if (getArguments()!= null && getArguments().containsKey("lista")){
            ArrayList<String> lista = getArguments().getStringArrayList("lista");

            adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, lista);
            lw.setAdapter(adapter);
        }
        return iv;
    }


}
