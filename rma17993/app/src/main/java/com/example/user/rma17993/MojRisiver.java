package com.example.user.rma17993;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MojRisiver extends BroadcastReceiver {

    WifiManager wifiManager;
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
            wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (wifiManager.getWifiState()  == WifiManager.WIFI_STATE_ENABLED || wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLING) {
                Toast.makeText(context.getApplicationContext(), "Automatic Message: WIFI  Enabled", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context.getApplicationContext(), "Automatic Message: WIFI Disabled", Toast.LENGTH_LONG).show();
            }
        }
    }

}
