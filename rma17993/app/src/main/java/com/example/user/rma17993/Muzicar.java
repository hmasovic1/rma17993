package com.example.user.rma17993;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.in;

public class Muzicar implements Parcelable, Serializable {
    private String naziv, zanr, webpage, biografija;
    private String slikaId;                         // id Muzicara spotify
    private String picture;
    private ArrayList<String> listaPjesama;
    public Muzicar(){
        listaPjesama = new ArrayList<String>();
    }

    public Muzicar(String id, String naziv, String zanr, String pic){
        this.slikaId = id;
        this.naziv = naziv;
        this.zanr = zanr;
        this.webpage = "";
        this.biografija = "";
        this.picture = pic;
        listaPjesama = new ArrayList<String>();
    }
    protected Muzicar(Parcel in) {
        slikaId = in.readString();
        naziv = in.readString();
        zanr = in.readString();
        webpage = in.readString();
        biografija = in.readString();
        picture = in.readString();
        in.readStringList(listaPjesama);
    }

    public Muzicar(String id, String naziv, String zanr, String webpage, String biografija){
        this.slikaId = id;
        this.naziv = naziv;
        this.zanr = zanr;
        this.webpage = webpage;
        this.biografija = biografija;
        listaPjesama = new ArrayList<String>();
    }

    public static final Creator<Muzicar> CREATOR = new Creator<Muzicar>() {
        @Override
        public Muzicar createFromParcel(Parcel parcel) {
            return new Muzicar(parcel);
        }

        @Override
        public Muzicar[] newArray(int i) {
            return new Muzicar[i];
        }
    };



    public void ubaciPjesmu(String pjesma) { listaPjesama.add(pjesma); }

    public ArrayList<String> dajListu(){return listaPjesama; }

    public String getIdSlike() { return slikaId; }

    public void setIdSlike(String ajdi) { this.slikaId = ajdi; }

    public String getNaziv() { return naziv; }

    public void setNaziv(String mName) { this.naziv = mName; }

    public String getZanr() { return zanr; }

    public void setZanr(String mName) { this.zanr = mName; }

    public String getWebpage() { return webpage; }

    public void setWebpage(String mName) { this.webpage= mName; }

    public String getBiografija() { return biografija; }

    public void setBiografija(String mName) { this.biografija = mName; }

    public String getPic() { return picture; }

    public void setPic(String ajdi) { this.picture = ajdi; }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(slikaId);
        parcel.writeString(naziv);
        parcel.writeString(zanr);
        parcel.writeString(webpage);
        parcel.writeString(biografija);
        parcel.writeString(picture);
        parcel.writeStringList(listaPjesama);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
