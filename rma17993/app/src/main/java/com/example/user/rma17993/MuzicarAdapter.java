package com.example.user.rma17993;


import android.content.Context;
import android.graphics.Movie;
import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MuzicarAdapter extends ArrayAdapter<Muzicar> {

    private Context mContext;
    private List<Muzicar> listaMuzicara = new ArrayList<>();

    public MuzicarAdapter(@NonNull Context context, ArrayList<Muzicar> list) {
        super(context, 0 , list);
        mContext = context;
        listaMuzicara = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null) listItem = LayoutInflater.from(mContext).inflate(R.layout.element_liste ,parent,false);

        Muzicar currentMovie = listaMuzicara.get(position);

        ImageView image = (ImageView)listItem.findViewById(R.id.ikona);

        String slika = currentMovie.getPic();
        Glide.with(getContext()).load(slika).into(image);

        TextView name = (TextView) listItem.findViewById(R.id.nazivMuzicara);
        name.setText(currentMovie.getNaziv());

        TextView release = (TextView) listItem.findViewById(R.id.zanrMuzicara);
        release.setText(currentMovie.getZanr());

        return listItem;
    }
}
