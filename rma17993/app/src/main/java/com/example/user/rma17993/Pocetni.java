package com.example.user.rma17993;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Locale;


public class Pocetni extends Activity implements FragmentLista.OnItemClick {

    private MuzicarAdapter muzikAdapter;
    private ArrayList<Muzicar> listaMuzikanata;
    private String abc = "";
    private Button dugme;
    private EditText textBox;
    private ListView lw;
    private AlertDialog.Builder bilder;

    private IntentFilter filter = new IntentFilter("ACTION_SEND");
    private MojRisiver receiver = new MojRisiver();

    private IntentFilter filter2 = new IntentFilter();
    private MojRisiver receiver2 = new MojRisiver();

    private Boolean siriL, drugiF;

    private static final int REQUEST_CODE = 1337;
    private static final String REDIRECT_URI = "yourcustomprotocol://callback";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_pocetni);
        registerReceiver(receiver, filter);

        filter2.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        registerReceiver(receiver2, filter2);


        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        String pamti = "";

        if (Intent.ACTION_SEND.equals(action) && type != null) if ("text/plain".equals(type)) pamti = intent.getStringExtra(Intent.EXTRA_TEXT);

        listaMuzikanata = new ArrayList<>();
       // Muzicar saban = new Muzicar(R.drawable.macka_1, "Šaban Šaulić", "Najjača muzika", "http://www.youtube.com", "Nema se šta reći..");
       // saban.ubaciPjesmu("Zal");
       // saban.ubaciPjesmu("Mihajlo");
       // saban.ubaciPjesmu("Sneg je opet snežana");

       // listaMuzikanata.add(saban);
        //listaMuzikanata.add(new Muzicar(R.drawable.macka_2, "Aca Lukas", "Pop"));


        // početak fragmenata

        siriL = false;

        FragmentManager fm = getFragmentManager();
        FrameLayout ldetalji = (FrameLayout)findViewById(R.id.mjestoF2);

        if(ldetalji != null){
            siriL = true;
            FragmentDetalji fd;
            fd = (FragmentDetalji)fm.findFragmentById(R.id.mjestoF2);

            if(fd == null){
                fd = new FragmentDetalji();
                fm.beginTransaction().replace(R.id.mjestoF2, fd).commit();
            }
        }

        FragmentLista fl = (FragmentLista)fm.findFragmentByTag("Lista");

        if(fl == null){
            fl = new FragmentLista();

            Bundle argumenti = new Bundle();

            argumenti.putParcelableArrayList("Alista", listaMuzikanata);
            fl.setArguments(argumenti);
            fm.beginTransaction().replace(R.id.mjestoF1, fl, "Lista").commit();
        }

        else {
            //slučaj kada mijenjamo orijentaciju uređaja
            //iz portrait (uspravna) u landscape (vodoravna)
            //a u aktivnosti je bio otvoren fragment FragmentDetalji
            //tada je potrebno skinuti FragmentDetalji sa steka
            //kako ne bi bio dodan na mjesto fragmenta FragmentLista

            // mjenjanje orijentacije ekrana
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        drugiF = false;




        /*
        dugme = (Button)findViewById(R.id.dugmic);
        textBox = (EditText)findViewById(R.id.textic);
        lw = (ListView)findViewById(R.id.listica);

        textBox.setText(pamti);

        listaMuzikanata = new ArrayList<>();
        Muzicar saban = new Muzicar(R.drawable.macka_1, "Šaban Šaulić", "Najjača muzika", "http://www.youtube.com", "Nema se šta reći..");
        saban.ubaciPjesmu("Zal");
        saban.ubaciPjesmu("Mihajlo");
        saban.ubaciPjesmu("Sneg je opet snežana");

        listaMuzikanata.add(saban);
        listaMuzikanata.add(new Muzicar(R.drawable.macka_2, "Aca Lukas", "Pop"));

        muzikAdapter = new MuzicarAdapter(this, listaMuzikanata);
        lw.setAdapter(muzikAdapter);

        dugme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                abc = textBox.getText().toString();
                if(abc.equals("")) return;
                listaMuzikanata.add(new Muzicar(R.drawable.macka_1, abc , "To be continued."));
                muzikAdapter.notifyDataSetChanged();
                textBox.setText("");
            }
        });


        bilder = new AlertDialog.Builder(this);

        lw.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                Muzicar trenutni = listaMuzikanata.get(pos);
                bilder.setMessage("Stranica: " + trenutni.getWebpage() + "\n" + "Biografija: " + trenutni.getBiografija());
                bilder.setTitle("Muzičar: " + trenutni.getNaziv());
                AlertDialog a = bilder.create();
                a.show();
                return true;
            }
        });


        lw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent myIntent = new Intent(Pocetni.this, detalji.class);
                myIntent.putExtra("obj", listaMuzikanata.get(i));
                Pocetni.this.startActivity(myIntent);
            }
        });
        */

    }


    @Override
    public void onItemClicked(int pos, ArrayList<Muzicar> m) {
        listaMuzikanata = m;
        //Priprema novog fragmenta FragmentDetalji
        Bundle arguments = new Bundle();
        arguments.putParcelable("muzicar", listaMuzikanata.get(pos));
        FragmentDetalji fd = new FragmentDetalji();
        fd.setArguments(arguments);
        if (siriL){
            //Slučaj za ekrane sa širom dijagonalom
            getFragmentManager().beginTransaction().replace(R.id.mjestoF2, fd).addToBackStack(null).commit();
        }
        else {
            //Slučaj za ekrane sa početno zadanom širinom
            getFragmentManager().beginTransaction().replace(R.id.mjestoF1, fd).addToBackStack(null).commit();
            //Primijetite liniju .addToBackStack(null)
        }

    }


}
