package com.example.user.rma17993;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class ResultRecieve extends ResultReceiver {

    private Receiver mReceiver;
    public ResultRecieve(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver re){
        mReceiver = re;
    }

        public interface Receiver {
            public void onReceiveResult(int resultCode, Bundle resultData);
        }


    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if(mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }
}
