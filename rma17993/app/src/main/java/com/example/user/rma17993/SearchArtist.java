package com.example.user.rma17993;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static com.spotify.sdk.android.authentication.LoginActivity.REQUEST_CODE;

public class SearchArtist extends AsyncTask<String, Integer, Void> {

    private String token;

    private ArrayList<Muzicar> listaDobavljenih = null;
    @Override
    protected Void doInBackground(String... strings) {

        String query = null;
        try {
            query = URLEncoder.encode(strings[0], "utf-8");
        } catch (UnsupportedEncodingException exep) {
            exep.printStackTrace();
            return null;
        }

         String noviURl = "https://api.spotify.com/v1/search?q=" + query + "&type=artist";
        // String noviURl = "https://api.myjson.com/bins/105f2n";



        String rezultat = null;
        URL url = null;
        try {
            url = new URL(noviURl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            token = "BQDE6SnrLhFO8-vpRGgLQzsKjj31vBhOmNfbsVdUvAUPT3mQ43N-HdB_8mzY3C3-XJw7X0HK5M1wiMJvl55ue-rWyJBeg6kcaMZ1RBN1KdWrIxp9K_t1CLTDMtrOpycowFQzRamF6kp7M4PNlKlJoHsDX-wWwm0";
            urlConnection.setRequestProperty("Authorization", "Bearer " + token);
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());



            rezultat = convertStreamToString(in);

        } catch (IOException e) {
            Log.d("id123", "UŠLO NE RADI");
                e.printStackTrace();
                return null;
        }



    JSONArray items = null;
        JSONArray slike = null;
        JSONArray genre = null;
        try {
            JSONObject jo = new JSONObject(rezultat);
            JSONObject artists = jo.getJSONObject("artists");
            items = artists.getJSONArray("items");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        listaDobavljenih = new ArrayList<Muzicar>();
        try {
            int broj = 1;
            for (int i = 0; i < items.length() && i < 5; ++i) {
                JSONObject lik = items.getJSONObject(i);
                String name = lik.getString("name");
                String id = lik.getString("id");

                genre = lik.getJSONArray("genres");

                slike = lik.getJSONArray("images");

                JSONObject slika = slike.getJSONObject(0);
                String zanr = genre.getString(0);


                String idSlika = slika.getString("url");
                Muzicar trenutni = new Muzicar(id,name,zanr, idSlika);

                listaDobavljenih.add(trenutni);
                broj++;
                // dodavanje muzicara u listu
            }
        }catch (JSONException e){
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public interface onMuzicarSearchDone {
        public  void onDone(ArrayList<Muzicar> lista);
    }

    private onMuzicarSearchDone poziv;
    public SearchArtist(onMuzicarSearchDone p, String b){
        poziv = p;
     //   token = b;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        poziv.onDone(listaDobavljenih);
    }


    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e){

        } finally {
            try {
                is.close();
            } catch (IOException e){
            }
        }
        return sb.toString();
    }

}
