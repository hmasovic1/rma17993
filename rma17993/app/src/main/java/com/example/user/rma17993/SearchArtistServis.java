package com.example.user.rma17993;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


public class SearchArtistServis extends IntentService {

    private final String token = "BQCvruJaRnJiopJUkiBtSPcxa438ChWe-QtrCkfIvpezETkiaTI451ZhofK90epl2CXUXvbQVjnrXOeJw8ZEdBoI-LyinaKGU6Q1k8uAQ_ZcxL9naPZSB1y0wk_2IDAPNbv-yVuMfVEEzrpdudBLmuPo_-wyj3Q";


    private ArrayList<Muzicar> listaDobavljenih = null;

    public SearchArtistServis(){
        super(null);
    }

    public SearchArtistServis(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        listaDobavljenih = new ArrayList<Muzicar>();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        //

        final ResultReceiver reciever = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();

        String strings = null;
        if(intent != null && intent.hasExtra("zahtjev")) {
            strings = intent.getStringExtra("zahtjev");
        }
        String query = null;
        try {
            query = URLEncoder.encode(strings, "utf-8");
        } catch (UnsupportedEncodingException exep) {
            exep.printStackTrace();
        }

        String noviURl = "https://api.spotify.com/v1/search?q=" + query + "&type=artist";
        // String noviURl = "https://api.myjson.com/bins/105f2n";


        String rezultat = null;
        URL url = null;
        try {
            url = new URL(noviURl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Authorization", "Bearer " + token);
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());


            rezultat = convertStreamToString(in);

        } catch (IOException e) {
            Log.d("id123", "UŠLO NE RADI");
            e.printStackTrace();
        }


        JSONArray items = null;
        JSONArray slike = null;
        JSONArray genre = null;
        try {
            JSONObject jo = new JSONObject(rezultat);
            JSONObject artists = jo.getJSONObject("artists");
            items = artists.getJSONArray("items");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        listaDobavljenih = new ArrayList<Muzicar>();
            int broj = 1;
            for (int i = 0; i < items.length() && i < 5; ++i)
            {
                try {
                JSONObject lik = items.getJSONObject(i);
                String name = lik.getString("name");
                String id = lik.getString("id");

                genre = lik.getJSONArray("genres");

                slike = lik.getJSONArray("images");

                JSONObject slika = slike.getJSONObject(0);
                String zanr = "NN";
                if(genre.length() !=0) zanr = genre.getString(0);


                String idSlika = slika.getString("url");
                Muzicar trenutni = new Muzicar(id, name, zanr, idSlika);

                listaDobavljenih.add(trenutni);
                broj++;
                // dodavanje muzicara u listu

            } catch(JSONException e){
                e.printStackTrace();
            }

        }

        bundle.putSerializable("result", listaDobavljenih);
        reciever.send(1, bundle);


    }



    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e){

        } finally {
            try {
                is.close();
            } catch (IOException e){
            }
        }
        return sb.toString();
    }
}

