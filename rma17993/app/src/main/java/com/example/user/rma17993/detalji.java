package com.example.user.rma17993;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.security.PrivateKey;

public class detalji extends Activity {

    private Muzicar muzo = new Muzicar();
    private TextView tv;
    private TextView tv2;
    private Button b;
    private ListView lw;
    private ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.lb_title_view);

        tv = (TextView)findViewById(R.id.textView);
        tv2 = (TextView)findViewById(R.id.textView2);
        muzo = (Muzicar) getIntent().getSerializableExtra("obj");

        String c = "Naziv muzičara: " + muzo.getNaziv() + "\n"
                + "Biografija: " + muzo.getBiografija() + "\n"
                + "Zanr: " + muzo.getZanr();

       tv.setText(c);

       tv2.setText("ClICK HERE -----> " + muzo.getWebpage());

       /*
       lw = (ListView)findViewById(R.id.listaPjesama);
       adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1 , muzo.dajListu());
       lw.setAdapter(adapter);  */



       tv2.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               String url = muzo.getWebpage();
               Intent intentic = new Intent(Intent.ACTION_VIEW);
               intentic.setData(Uri.parse(url));
               startActivity(intentic);
           }
       });

       b = (Button)findViewById(R.id.button2);
       b.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               // Kreiranje tekstualne poruke
               Intent sendIntent = new Intent();
               sendIntent.setAction(Intent.ACTION_SEND);
               sendIntent.putExtra(Intent.EXTRA_TEXT, "Imate čast dobiti poruku poslanu sa RMA-17XXX aplikacije. Long live iShabban v1.02!");
               sendIntent.setType("text/plain");
               // Provjera da li postoji aplikacija koja može obaviti navedenu akciju
               if (sendIntent.resolveActivity(getPackageManager()) != null) {
                   startActivity(sendIntent);
               }
           }
       });

        lw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // https://www.youtube.com/results?search_query=asda
                String url = "https://www.youtube.com/results?search_query=";
                Intent intentic = new Intent(Intent.ACTION_VIEW);

                String trenutni = muzo.dajListu().get(i);
                intentic.setData(Uri.parse(url + trenutni));
                startActivity(intentic);
            }
        });



    }

}

